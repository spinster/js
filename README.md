# Javascript
This homework was slapped together haphazardly from class exercises. I do not consider myself to have
any self-mastery of this subject, and will attempt to work on this in the future, as needed.

I will say, however that the Brackets editor is wonderful. I learned at least that much from this.
